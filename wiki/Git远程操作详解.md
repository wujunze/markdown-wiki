Git是目前最流行的版本管理系统，学会Git几乎成了开发者的必备技能。

Git有很多优势，其中之一就是远程操作非常简便。本文详细介绍5个Git命令，它们的概念和用法，理解了这些内容，你就会完全掌握Git远程操作。

- [git clone][1]
- [git remote][2]
- [git fetch][3]
- [git pull][4]
- [git push][5]

本文针对初级用户，从最简单的讲起，但是需要读者对Git的基本用法有所了解。同时，本文覆盖了上面5个命令的几乎所有的常用用法，所以对于熟练用户也有参考价值。

[1]:/Home/Wiki/index.html#view-258
[2]:/Home/Wiki/index.html#view-259
[3]:/Home/Wiki/index.html#view-260
[4]:/Home/Wiki/index.html#view-261
[5]:/Home/Wiki/index.html#view-262

