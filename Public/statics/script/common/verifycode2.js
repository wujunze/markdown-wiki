define("common/verifycode2.js", ['jquery'], function(e, t, n) {
	"use strict";
	var jQuery = e('jquery');

	var g = (function($) {

		var that = {};
		var $dom = $('.verifycode');
		var $img = $dom.find("img");
		var $null = $dom.find(".jsVerifyNull");
		var $error = $dom.find(".jsVerifyError");
		var $input = $dom.find("input");
		$dom.find("a").click(function(e) {
			that.refresh();
		});
		$img.click(function(e) {
			that.refresh();
		});
		$input.keyup(function(e) {
			if ($input.val().trim().length > 0) {
				$null.hide(), $error.hide();
				var k = "which" in e ? e.which : e.keyCode;
				k == 13 && t && t($(this).val());
			} else $null.show();
		});
		that.val = function() {
			$null.hide();
			$input.val() == "" && $null.show();
			return $input.val();
		};
		that.err = function() {
			return $error.show();
		};
		that.refresh = function() {
			if($img.length) {
				var s = $img.attr("src");
				$img.attr("src", s.split('?')[0] + '?r=' +(new Date).getTime()), $input.val(""), $error.hide(), $null.hide();
			}
		};
		that.refresh();

		return that;
	}(jQuery));

	n.exports = g;
});