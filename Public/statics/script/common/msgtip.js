define(function(require, exports, module) {

    var i = require('asset/layer/layer');
    i.config({
        path: seajs.resolve('asset/layer/'), //layer.js所在的目录，可以是绝对目录，也可以是相对目录
    });
    i.suc = function(s, t) {
        s = s || '操作成功';
        t = t || 2500; //自动关闭的毫秒数
        /*
        i.alert(s, {
            icon: 1,
            shadeClose: true,
            title: '系统提示'
        });
        */
        i.msg(s, {
            time: t,
            icon: 1
        });
    };
    i.err = function(s, t, c) {
        s = s || '操作失败';
        t = t || 2500; //自动关闭的毫秒数
        c = c || 2;
        /*
        i.alert(s, {
            icon: c,
            shadeClose: true,
            title: '系统提示'
        });
        */
        i.msg(s, {
            time: t,
            icon: c,
        });
    };
    i.tip = function(s, t) {
        t = t || 2500; //自动关闭的毫秒数
        i.msg(s, {
            offset: 'auto',
            time: t,
        });
    }
    //return i;
    module.exports = i;

});

