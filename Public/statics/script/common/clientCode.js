define("common/clientCode.js", [ "common/msgtip.js" ], function(e, t, n) {
    "use strict";
    var $ = e("jquery");
    Function.prototype.delay = function(timeout) {
        var __method = this,
            args = Array.prototype.slice.call(arguments, 1);
        timeout = timeout * 1000;
        return window.setTimeout(function() {
            return __method.apply(__method, args);
        }, timeout);
    };
    function r(e) {
        var t = this, n = {
            from: $("#from"),
            to: $("#to"),
            send: $("#send"),
            url: "",
            callback: null,
            rule: function() {
                return !0;
            },
            name: "手机号",
            time: 60
        };
        this.opt = $.extend(!0, n, e), this.$from = this.opt.from, this.$to = this.opt.to, this.$send = this.opt.send, this.$to.attr("disabled", !0).parent().addClass("disabled"), this.$send.click(function(e) {
            var n = this, r = t.opt;
            if ($(this).hasClass("btn_disabled")) return !1;
            if (!(t.$from.val().trim().length > 0)) return u.err(r.name + "不能为空"), !1;
            if (!r.rule(t.$from.val().trim())) return u.err(r.name + "格式不合法"), !1;
            t.$from.prop("disabled", !0).parent().addClass("disabled");
            $(this).addClass("btn_disabled");
            $.get(r.url + t.$from.val().trim(), function(e) {
                if(typeof r.callback == "function") {
                    r.callback(e) === !0 && i.call(t, e);
                }
                else {
                    i.call(t, e);
                    $(n).parent().removeClass("btn_disabled");
                }
            });
        });
    }
    function i(e) {
        if(e.ret == 0) {
            this.$to.prop("disabled", !1).focus().parent().removeClass("disabled");
            s.delay(1, this.$send, this.$from, this.opt.time);
        }
        else {
            u.err();
            this.$from.prop("disabled", !1).parent().removeClass("disabled");
            this.$send.removeClass("btn_disabled");
        }
    }
    function s(e, t, n) {
        e.text("重新发送(" + n + ")");
        if(n > 0) {
            s.delay(1, e, t, n - 1)
        }
        else {
            e.text("获取验证码");
            t.prop("disabled", !1).parent().removeClass("disabled");
            e.removeClass("btn_disabled");
        }
    }
    var u = e("common/msgtip.js");
    r.prototype = {
        val: function() {
            return this.$from.val().trim().length > 0 ? this.opt.rule(this.$from.val().trim()) ? this.$from.val().trim() : (u.err(this.opt.name + "格式不合法"), !1) : (u.err(this.opt.name + "不能为空"), !1);
        },
        code: function() {
            return this.$to.val().trim().length > 0 ? this.$to.val().trim() : (u.err(this.opt.name + "验证码不能为空"), !1);
        }
    };
    n.exports = r;
});