define("jquery/plugins/jquery.cookie.js", [], function(e, t, n) {
    "use strict";
    var $ = e('jquery');

    (function(e, t, n) {
        function r(e) {
            return e;
        }

        function i(e) {
            return s(decodeURIComponent(e.replace(u, " ")));
        }

        function s(e) {
            return e.indexOf('"') === 0 && (e = e.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\")), e;
        }

        function o(e) {
            return a.json ? JSON.parse(e) : e;
        }
        var u = /\+/g,
            a = e.cookie = function(s, u, f) {
                if (u !== n) {
                    f = e.extend({}, a.defaults, f), u === null && (f.expires = -1);
                    if (typeof f.expires == "number") {
                        var l = f.expires,
                            c = f.expires = new Date;
                        c.setDate(c.getDate() + l);
                    }
                    return u = a.json ? JSON.stringify(u) : String(u), t.cookie = [encodeURIComponent(s), "=", a.raw ? u : encodeURIComponent(u), f.expires ? "; expires=" + f.expires.toUTCString() : "", f.path ? "; path=" + f.path : "", f.domain ? "; domain=" + f.domain : "", f.secure ? "; secure" : ""].join("");
                }
                var h = a.raw ? r : i,
                    p = t.cookie.split("; "),
                    d = s ? null : {};
                for (var v = 0, m = p.length; v < m; v++) {
                    var g = p[v].split("="),
                        y = h(g.shift()),
                        b = h(g.join("="));
                    if (s && s === y) {
                        d = o(b);
                        break;
                    }
                    s || (d[y] = o(b));
                }
                return d;
            };
        a.defaults = {}, e.removeCookie = function(t, n) {
            return e.cookie(t) !== null ? (e.cookie(t, null, n), !0) : !1;
        };
    })($, document);

});