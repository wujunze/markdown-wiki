define(function(require, exports, module) {
	//var $ = require('jquery');
	var Handlebars = require('handlebars');

	//注册一个比较大小的Helper: compare
	Handlebars.registerHelper('compare', function(left, operator, right, options) {
		if (arguments.length < 3) {
			throw new Error('Handlerbars Helper "compare" needs 3 parameters');
		}
		var operators = {
			'==':     function(l, r) {return l == r; },
			'===':    function(l, r) {return l === r; },
			'!=':     function(l, r) {return l != r; },
			'!==':    function(l, r) {return l !== r; },
			'<':      function(l, r) {return l < r; },
			'>':      function(l, r) {return l > r; },
			'<=':     function(l, r) {return l <= r; },
			'>=':     function(l, r) {return l >= r; },
			'typeof': function(l, r) {return typeof l == r; }
		};

		if (!operators[operator]) {
			throw new Error('Handlerbars Helper "compare" doesn\'t know the operator ' + operator);
		}

		var result = operators[operator](left, right);

		if (result) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	});

	//注册一个比较相等的Helper: equal
	Handlebars.registerHelper('equal', function(left, right, options) {
		if(left == right){
			return options.fn(this);
		}else{
			return options.inverse(this);
		}
	});

	module.exports = Handlebars;

});