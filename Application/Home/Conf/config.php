<?php
return array(
	//'配置项'=>'配置值'

    'URL_MODEL' => 2,

    /* 自定义模板的字符串替换 */
    'TMPL_PARSE_STRING'=>array(
        '__PUBLIC__'=>__ROOT__.'/Public',
    ),
    'TAGLIB_BEGIN'          => '<',
    'TAGLIB_END'            => '>',

    /* 语言设置 */
    'LANG_SWITCH_ON'        =>  true,       //开启语言包功能
    'LANG_AUTO_DETECT'      =>  true,       // 自动侦测语言
    'DEFAULT_LANG'          =>  'zh-cn',    // 默认语言

    // Session自动开启
    'SESSION_AUTO_START'    => TRUE,
    'SESSION_OPTIONS'       =>  array(
        'name'              =>  'WIKISESSION',    //设置session名
        'expire'            =>  24*3600*1,        //SESSION保存1天
        'use_trans_sid'     =>  1,                //跨页传递
        'use_only_cookies'  =>  0,                //是否只开启基于cookies的session的会话方式
    ),

    /* 分页设置 */
    'VAR_PAGE' => 'page', // 分页标识符
    'PAGE_SIZE' => 10, //每页显示数量
);