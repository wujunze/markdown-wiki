<?php
namespace Home\Controller;
class BaseController extends \Think\Controller{

    protected $err;
    protected $_AreaMod;

    //构造函数
    protected function _initialize() {
        //初始化模型
        $this->_AreaMod = D('Area');
        //设置头信息
        $this->setHeader();
        //设置公共信息
        $this->setInfo();
        //设置错误信息
        $this->initErr();
    }

    //错误信息
    public function initErr() {
        $this->err = array();
        /* 常用 */
        $this->err['0'] = '操作成功';
        $this->err['-1'] = '操作失败';
    }

    public function setHeader() {
        //设置此页面的过期时间(用格林威治时间表示)
        header("Expires: Mon, 26 Jul 1970 05:00:00 GMT");
        //设置此页面的最后更新日期(用格林威治时间表示)为当天
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        //告诉客户端浏览器不使用缓存，HTTP 1.1 协议
        header("Cache-Control: no-cache, must-revalidate");
        //告诉客户端浏览器不使用缓存，兼容HTTP 1.0 协议
        header("Pragma: no-cache");
        //设置浏览器编码
        header("Content-type: text/html; charset=utf-8");
    }

    public function setInfo() {
        $sys['title'] = C('WEBNAME');
        $sys['name'] = C('WEBNAME');
        $this->assign('sys', $sys);
    }

    //检查登陆
    public function checkLogin() {
        //验证后台登录权限
        if(!$this->checkAuthorized()) {
            $this->error("没有操作权限", U('Index/index'));
        }
    }

    //检测登陆权限
    protected function checkAuthorized() {
        return true;
    }

    // 检测输入的验证码是否正确，$code为用户输入的验证码字符串
    protected function correct_verify($code, $id = ''){
        $verify = new \Think\Verify();
        return $verify->check($code, $id);
    }

    //校验验证码
    // 检测输入的验证码是否正确，$code为用户输入的验证码字符串
    protected function check_code($code, $id = ''){
        if(empty($code)) {
            $this->setError(1000);
        }
        if(!$this->correct_verify($code, $id)){
            $this->setError(1001);
        }
        return true;
    }

    // 设置错误返回信息至浏览器
    protected function setError($n, $type = 0) {
        $n && $this->recordCookie($type);
        $res = $this->errCode((int) $n);
        if($n==0) {
            $this->success($res['desc'],'',$res);
        }
        else {
            $this->error($res['desc'],'',$res);
        }
        exit;
    }

    // 获取错误信息
    protected function errCode($n) {
        $data = array(
            'ret'=>$n,
            'desc'=>$this->err[(int) $n],
        );
        return $data;
    }

    // 添加错误信息
    protected function addErr($n, $d) {
        $this->err[$n] = $d;
    }

    // 抛出提示输入验证码
    protected function checkCookie($type = 0, $field = '') {
        $field = $field ? $field : 'verify_code';
        $errCount = 0;
        if($type==1) {
            $errCount = (int) cookie('m_login');
        }
        else if($type==2){
            $errCount = (int) cookie('m_login');
        }
        else if($type==3){
            $errCount = (int) cookie('m_pwd');
        }
        if($errCount>=3) {
            //校验验证码
            $verify = I($field);
            $this->check_code($verify);
        }
    }

    //记录错误次数
    protected function recordCookie($type = 0) {
        if($type==1) {
            $n = (int) cookie('m_login');
            cookie('m_login', $n+1, 24*3600);
        }
        else if($type==2){
            $n = (int) cookie('m_register');
            cookie('m_register', $n+1, 24*3600);
        }
        else if($type==3){
            $n = (int) cookie('m_pwd');
            cookie('m_pwd', $n+1, 24*3600);
        }
    }

    //清除记录错误次数
    protected function clearCookie($type = 0) {
        if($type==1) {
            cookie('m_login', null);
        }
        else if($type==2){
            cookie('m_register', null);
        }
        else if($type==3){
            cookie('m_pwd', null);
        }
    }

    //获取区域名称
    protected function getArea($id) {
        $area_name = '';
        if($id) {
            $r = $this->_AreaMod->where(array('id'=>$id))->find();
            if($r) {
                $area_name = $r['shortname'] ? $r['shortname'] : $r['areaname'];
                $area_name = str_replace(array('省', '市'), '', $area_name);
            }
        }
        return $area_name;
    }

}
