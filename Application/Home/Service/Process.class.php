<?php
namespace Home\Service;
class Process {

    /**
     * 返回一个实例
     */
    static public function getInstance() {
        static $handier = NULL;
        if (empty($handier)) {
            $handier = new static();
        }
        return $handier;
    }

}
