<?php
namespace Admin\Controller;
class RoleController extends BaseController{

    //模型
    private $_mod;

    //构造函数
    public function _initialize() {
        parent::_initialize();
        $this->_mod = D('Role');
    }

    //角色列表
    public function index()
    {
        $AdminRole = $this->_mod->where("admin=1")->select();
        $this->assign('data', $AdminRole);
        $this->display();
    }

    //添加角色
    public function add()
    {
        if (IS_POST) {
            if ($this->_mod->addRole()) {
                $this->success('添加成功');
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $this->assign('res', null);
            $this->display('edit');
        }
    }

    //编辑角色
    public function edit()
    {
        if (IS_POST) {
            if ($this->_mod->editRole()) {
                $this->success('修改成功');
            } else {
                $this->error($this->_mod->getError());
            }
        } else {
            $rid = I('rid', 0, 'intval');
            if ($rid) {
                $res = $this->_mod->find($rid);
                $this->assign('res', $res);
                $this->display();
            }
        }
    }

    //删除角色
    public function del()
    {
        if ($this->_mod->delRole()) {
            $this->success('删除角色成功！');
        } else {
            $this->error('参数错误');
        }
    }
}