<?php
namespace Admin\Controller;
class PublicController extends BaseController{
    public function _initialize() {
        parent::_initialize();
    }

    public function login(){
        if(IS_POST) {
            $username = I("post.username", "", "trim");
            $password = I("post.password", "", "trim");
            if (empty($username) || empty($password)) {
                $this->error("用户名或者密码不能为空！", U("login"));
            }
            if (isset($_POST['code'])) {
                $code = I("post.code", "", "trim");
                if((empty($code))) {
                    $this->error("请输入验证码！", U("login"));
                }
                //验证码开始验证
                if (!$this->verify($code)) {
                    $this->error("验证码错误，请重新输入！", U("login"));
                }
            }
            if ($this->adminUser->login($username, $password)) {
                $forward = cookie("forward");
                if (!$forward) {
                    $forward = U("Index/index");
                } else {
                    cookie("forward", NULL);
                }
                //redirect(U('Index/index'));
                $this->success('登陆成功',U('Index/index'));
            } else {
                $this->error($this->adminUser->error ? $this->adminUser->error : '登陆失败', U("login"));
            }
        }
        else {
            //如果已经登录
            if ($this->adminUser->isLogin()) {
                redirect(U('Index/index'));
            }
            $this->display();
        }
    }

    public function logout() {
        if ($this->adminUser->logout()) {
            //手动登出时，清空forward
            cookie("forward", NULL);
            $this->success('注销成功！', U('login'));
        }
    }

    /**
     * 验证码验证
     */
    static public function verify($code) {
        return true;
    }

    /**
     * 获取菜单
     */
    public function menu() {
        //获取菜单
        $this->getMenu();
        $this->display();
    }

}
