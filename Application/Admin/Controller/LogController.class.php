<?php
namespace Admin\Controller;
class LogController extends BaseController{

    private $_loginlogMod;

    public function _initialize() {
        parent::_initialize();
        $this->_loginlogMod = D('LoginLog');
    }

    /**
     * 后台登陆日志
     */
    public function loginlog()
    {
        if (IS_POST) {
            $data = array('m'=>MODULE_NAME, 'c'=>CONTROLLER_NAME, 'a'=>ACTION_NAME);
            $data = array_merge($data, $_POST);
            $url = __ROOT__ . "/index.php?" . http_build_query($data);
            redirect($url);
        }
        $where = array();
        $username = I('username');
        $start_time = I('start_time');
        $end_time = I('end_time');
        $loginip = I('loginip');
        $status = I('status');
        if (!empty($username)) {
            $where['username'] = array('like', '%' . $username . '%');
        }
        if (!empty($start_time) || !empty($end_time)) {
            if (!empty($start_time) && !empty($end_time)) {
                $start_time = strtotime($start_time);
                $end_time = strtotime($end_time) + 86399;
                $where['logintime'] = array('BETWEEN', array($start_time, $end_time));
            } else if(!empty($start_time)) {
                $start_time = strtotime($start_time);
                $where['logintime'] = array('GT', $start_time);
            } else {
                $end_time = strtotime($end_time) + 86399;
                $where['logintime'] = array('LT', $end_time);
            }
        }
        if (!empty($loginip)) {
            $where['loginip'] = array('like', "%{$loginip}%");
        }
        if ($status != '') {
            $where['status'] = $status;
        }
        $count = $this->_loginlogMod->where($where)->count();
        if (! empty ( $_REQUEST ['pagesize'] )) {
            $listRows = $_REQUEST ['pagesize'];
        } else {
            $listRows = C('PAGE_SIZE') ? C('PAGE_SIZE') : 10;
        }
        $page = $this->page($count, $listRows);
        $data = $this->_loginlogMod->where($where)->limit($page->firstRow . ',' . $page->listRows)->order(array('id' => 'DESC'))->select();
        $this->assign("data", $data);
        //分页显示
        $this->assign('page', $page->show());
        $this->assign('where', $where);
        $this->display();
    }

    //删除登陆日志
    public function delLoginLog()
    {
        $id = I("id", 0, "trim");
        if ($this->_loginlogMod->delete($id)) {
            $this->success('删除成功');
        } else {
            $this->success('操作失败');
        }
    }

    //删除一个月前的登陆日志
    public function delMonthLoginLog() {
        if ($this->_loginlogMod->deleteLog(0, 30, 'd')) {
            $this->success("删除登陆日志成功！", U('loginlog'));
        } else {
            $this->error("删除登陆日志失败！");
        }
    }

}