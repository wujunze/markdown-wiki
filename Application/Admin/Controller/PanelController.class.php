<?php
namespace Admin\Controller;
class PanelController extends BaseController{

    private $_Mod;

    public function _initialize() {
        parent::_initialize();
        $this->_Mod = D('Panel');
    }

    //设置常用菜单
    public function set()
    {
        $prefix = C('DB_PREFIX');
        if (IS_POST) {
            if($this->_Mod->setPanel($this->adminUser->uid)) {
                $this->success('常用菜单设置成功');
            }
            else {
                $error = $this->_Mod->getError() ? $this->_Mod->getError() : '操作失败';
                $this->error($error);
            }
        } else {
            if ($this->adminUser->isAdministrator()) {
                $sql = "SELECT n.nid,n.pid,m.uid,n.title FROM {$prefix}node AS n  LEFT JOIN
							 (SELECT * FROM {$prefix}panel WHERE uid={$this->adminUser->uid}) AS m
							 ON n.nid = m.nid WHERE n.is_show=1";
            } else {
                $sql = "SELECT n.nid,n.pid,m.uid,n.title FROM {$prefix}node AS n  LEFT JOIN  {$prefix}access AS a ON n.nid=a.nid LEFT JOIN
							 (SELECT * FROM {$prefix}panel WHERE uid={$this->adminUser->uid}) AS m ON n.nid = m.nid
							 WHERE n.type=2 OR (n.is_show=1 AND a.nid is not null)";
            }
            $nodeData = $this->_Mod->query($sql);
            $nodeData = \Common\Lib\Tool\Data::channelLevel($nodeData, 0, "", "nid");
            $this->assign('data', $nodeData);
            $this->display();
        }
    }

}