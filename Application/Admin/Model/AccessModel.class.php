<?php
namespace Admin\Model;
class AccessModel extends \Think\Model{

    public $tableName = 'access';

    public function editAccess()
    {
        $rid = I('rid', 0, 'intval');
        if ($rid) {
            $menuid = I('post.menuid');
            //删除权限
            M($this->tableName)->where(array("rid" => $rid))->delete();
            $mids = D('Node')->getMenuIds();
            if($mids) {
                M($this->tableName)->where(array("nid" => array('IN', $mids)))->delete();
            }
            foreach (explode(',',$menuid) as $v) {
                if($v) {
                    $data = array("rid" => $rid, "nid" => intval($v));
                    $this->add($data);
                }
            }
            return true;
        } else {
            $this->error = '参数错误';
            return false;
        }
    }
}