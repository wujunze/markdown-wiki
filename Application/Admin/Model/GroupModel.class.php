<?php
namespace Admin\Model;
class GroupModel extends \Think\Model{

    //角色表
    public $tableName = 'role';
    public $userTable = "user";

    //自动完成
    protected $_auto = array(
        array('creditslower', 'intval', 3, 'function'),
    );

    //验证
    protected $_validate = array(
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        array('rname', 'require', '组名不能为空'),
        array('rname','','会员组已经存在', 0, 'unique', 3),
        array('creditslower', 'require', '积分不能为空'),
    );

    //添加会员组
    public function addRole()
    {
        if ($this->create()) {
            if ($this->add()) {
                return true;
            } else {
                $this->error = '添加失败';
            }
        }
    }

    //修改会员组
    public function editRole()
    {
        if ($this->create()) {
            if (false !== $this->save()) {
                return true;
            } else {
                $this->error = '修改失败';
            }
        }
    }

    //删除组
    public function delRole()
    {
        $rid = I('rid', 0, 'trim');
        if ($this->delete($rid)) {
            M($this->userTable)->where(array( 'rid' => ['IN',explode(',',$rid)] ))->save(array('rid' => 4));
            return true;
        } else {
            $this->error = '删除失败';
        }
    }
}
